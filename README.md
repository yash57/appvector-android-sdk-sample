# AppVector Android SDK

## Integration Steps
Before Integrating the SDK, update the Gradle version to 4.2.2 or the latest version available
Add the following


1. gradle.properties  
   ```authToken=PLACE_REPO_AUTH_TOKEN```


2. Project Level Gradle
    ```groovy
    allprojects {
    repositories {
           google()
           jcenter()
           maven { url 'https://jitpack.io'
               credentials { username authToken }
           }
       }
    }
    ```

3. App Level Gradle
    ```groovy
    implementation 'com.google.android.gms:play-services-ads-identifier:17.0.1'
    implementation 'ai.multivariate:multivariate-android-sdk:0.4.8'
    ```


4. Manifest File

   Declare the below android name in the application tag
      ```android:name="com.multivariate.multivariate_core.Application" ```

   LOG refers to the Debug Logcat
      ```
      <meta-data
         android:name="MULTIVARIATE_ID"
         android:value="ENTER_MV_URL_HERE"/>
      <meta-data
         android:name="MULTIVARIATE_TOKEN"
         android:value="ENTER_SERVER_TOKEN_HERE"/>
      <meta-data
         android:name="LOG"
         android:value="false"/>
      <service android:name="com.multivariate.multivariate_core.notifications.FCMService"
         android:exported="false">
         <intent-filter>
            <action android:name="com.google.firebase.MESSAGING_EVENT"/>
         </intent-filter>
      </service>
   
      ```

5. Manifest Permissions
   ```
       <uses-permission android:name="android.permission.INTERNET"/>
       
       <!-- If the application uses location -->
       <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
       <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
   ```


6. Proguard-rules
   ```
       -keep class com.multivariate.** { *; }
       -dontwarn com.multivariate.** 
   ```


## Custom Initialization

1. Add the below to the existing Application class
   ```java
   public class App extends Application {
       @Override
       public void onCreate() {
           super.onCreate();
           try {
               //Initialize the Notification Manager
               MVNotificationManager.init(this);
               //Initialize the MultivariateAPI
               new MultivariateAPI.Builder(this)
                       .setHost(BuildConfig.ID)
                       .setToken(BuildConfig.SERVER_TOKEN)
                       .setLogEnable(false)
                       .build();
           } catch (Exception e) {
               Log.d("APPLICATION", "Error Init " + e.getMessage());
           }
       }
   }
   ```


## Event Logging
```kotlin
    MultivariateAPI.getInstance().pushEvent("status", 123)
    MultivariateAPI.getInstance().pushEvent("1231")
```
