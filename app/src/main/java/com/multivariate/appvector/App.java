package com.multivariate.appvector;

import android.app.Application;
import android.util.Log;

import com.multivariate.multivariate_core.MultivariateAPI;
import com.multivariate.multivariate_core.notifications.MVNotificationManager;

/**
 * Replace the name in the Manifest with this Application
 * class when custom initialization is required.
 *
 * **/
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            //Initialize the Notification Manager
            MVNotificationManager.init(this);
            //Initialize the MultivariateAPI
            new MultivariateAPI.Builder(this)
                    .setHost(BuildConfig.ID)
                    .setToken(BuildConfig.SERVER_TOKEN)
                    .setLogEnable(false)
                    .build();
        } catch (Exception e) {
            Log.d("APPLICATION", "Error Init " + e.getMessage());
        }
    }
}
