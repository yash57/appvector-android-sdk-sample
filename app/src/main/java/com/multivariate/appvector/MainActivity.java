package com.multivariate.appvector;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.multivariate.multivariate_core.MultivariateAPI;

public class MainActivity extends AppCompatActivity {

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MultivariateAPI.getInstance().pushEvent("Visited Main");

        button = findViewById(R.id.button_main);
        button.setOnClickListener(view -> {
            MultivariateAPI.getInstance().pushEvent("clicked", "true");
        });
    }
}